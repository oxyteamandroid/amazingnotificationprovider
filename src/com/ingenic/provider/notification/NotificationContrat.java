/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * 消息通知合约类
 * 
 * @author tZhang
 * 
 */
public class NotificationContrat {
    public static final String AUTHORITY = "com.ingenic.notification";
    public static final String CALLER_IS_SYNCADAPTER = "caller_is_syncadapter";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String ACTION_NOTIFICATION_UPDATE = "com.ingenic.launcher.notification.NOTIFICATION_UPDATE";

    private NotificationContrat() {
    }

    private interface NotificationColumns {
        String APPNAME = "appName";
        String PACKAGENAME = "packageName";
        String TITLE = "title";
        String MESSAGE = "message";
        String UPDATE_TIME = "time";
        String READED = "readed";
    }

    public static final class Notifications implements BaseColumns,
            NotificationColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://"
                + AUTHORITY + "/notifications");
        public static final String DEFAULT_SORT_ORDER = READED + " DESC";
        public static final String[] PROVIDER_WRITABLE_COLUMNS = new String[] {
                APPNAME, PACKAGENAME, TITLE, MESSAGE, UPDATE_TIME, READED };

        private Notifications() {
        };
    }
}
