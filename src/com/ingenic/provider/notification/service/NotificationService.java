/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.app.Note;
import com.ingenic.iwds.app.NotificationProxyServiceManager;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.iwds.datatransactor.elf.NotificationTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.provider.notification.NotificationContrat;
import com.ingenic.provider.notification.NotificationDatabaseHelper;
import com.ingenic.provider.notification.utils.NotificationProviderUtils;

public class NotificationService extends Service implements
        ServiceClient.ConnectionCallbacks {
    private static final String TAG = "NotificationService";

    private static final String UUID = "5a177e43-82e6-d483-b7e3-d7072047e346";

    private static NotificationTransactionModel mNotificationTransactionModel;

    private NotificationDatabaseHelper mDbHelper;

    private Intent mUpdateIntent;

    private ServiceClient m_client;
    private NotificationProxyServiceManager m_service;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();

        // 使得服务不得被杀死
        CharSequence label = getApplication().getApplicationInfo().loadLabel(
                getPackageManager());

        Notification notification = new Notification(getApplication()
                .getApplicationInfo().icon, label, System.currentTimeMillis());

        Intent it = new Intent(
                ServiceManagerContext.ACTION_NOTIFICATION_CLICKED);
        it.setFlags(it.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, it, 0);
        notification.setLatestEventInfo(this, label, "", pendingIntent);
        notification.flags = notification.flags
                | Notification.FLAG_ONGOING_EVENT;

        startForeground(9998, notification);

        // 初始化数据库助手
        mDbHelper = NotificationDatabaseHelper.getInstance(this);

        // 初始化接收通知消息的传输模型
        if (mNotificationTransactionModel == null) {
            mNotificationTransactionModel = new NotificationTransactionModel(
                    this, mModelCallback, UUID);
        }

        IwdsLog.d(TAG, "Notification - TransactionModel is started!");
        mNotificationTransactionModel.start();

        // 连接服务通讯(用于向Launcher发送消息的服务通讯)
        if (m_client == null) {
            m_client = new ServiceClient(this,
                    ServiceManagerContext.SERVICE_NOTIFICATION_PROXY, this);
            m_client.connect();
        }

        // 启动消息中心的Intent
        intent = new Intent();
        ComponentName componentName = new ComponentName(
                "com.ingenic.notification",
                "com.ingenic.notification.NotificationActivity");
        intent.setComponent(componentName);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_DATE_CHANGED);
        registerReceiver(mReceiver, filter);
    }

    private Intent intent;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }

        if (mNotificationTransactionModel != null) {
            mNotificationTransactionModel.stop();
        }

        // 在异常退出时保证服务畅通
        sendBroadcast(new Intent("ingenic.intent.action.RESTART_SERVICE"));
    }

    private NotificationTransactionModel.NotificationTransactionModelCallback mModelCallback = new NotificationTransactionModel.NotificationTransactionModelCallback() {

        @Override
        public void onSendResult(DataTransactResult result) {

        }

        @Override
        public void onRequestFailed() {

        }

        @Override
        public void onRequest() {

        }

        @Override
        public void onObjectArrived(NotificationInfo object) {
            IwdsLog.i(TAG,
                    "Notification - Receive Object is:" + object.toString());

            long _id = -100;
            if (object != null) {
                // 保存数据
                if (mDbHelper != null) {
                    _id = mDbHelper.notificationInsert(object);
                }

                if (_id != -1) {
                    if (mUpdateIntent == null) {
                        // 用于更新消息中心的广播Intent
                        mUpdateIntent = new Intent(
                                NotificationContrat.ACTION_NOTIFICATION_UPDATE);
                    }

                    object.id = _id;
                    mUpdateIntent.putExtra("notification", object);// 待查证，这里ID=-1与实际不符
                    mUpdateIntent.putExtra("_id", _id);
                    sendBroadcast(mUpdateIntent);

                    // 不在消息中心界面时提示用户
                    if (!NotificationProviderUtils.isHome(getApplicationContext())) {
                        // 播放提示音
                        try {
                            RingtoneManager
                                    .getRingtone(
                                            getApplicationContext(),
                                            RingtoneManager
                                                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                    .play();
                        } catch (Exception e) {
                            IwdsLog.d(TAG, "rington play error!");
                        }

                        // 震动一下
                        if (enableVibrator) {
                            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(new long[] { 0, 500, 1000, }, -1);
                            enableVibrator = false;
                            mHandler.sendEmptyMessageDelayed(0, 5000);
                        }

                        // 更新launcher
                        IwdsLog.i(TAG, "UPDATE LAUNCHER STATUSBAR");
                        if (m_service != null) {
                            intent.putExtra("id", _id);
                            Note note = new Note(
                                    object.title,
                                    object.content,
                                    PendingIntent
                                            .getActivity(
                                                    getApplicationContext(),
                                                    Intent.FLAG_ACTIVITY_FORWARD_RESULT,
                                                    intent,
                                                    PendingIntent.FLAG_UPDATE_CURRENT));
                            m_service.notify((int) _id, note);
                        }
                    }

                }

            }

        }

        @Override
        public void onLinkConnected(DeviceDescriptor descriptor,
                boolean isConnected) {
            IwdsLog.i(TAG, "Notification - DeviceDescriptor :" + descriptor
                    + " connect is " + isConnected);
        }

        @Override
        public void onChannelAvailable(boolean isAvailable) {
            IwdsLog.i(TAG, "Notification - Channel Available is:" + isAvailable);
        }
    };

    @Override
    public void onConnected(ServiceClient serviceClient) {
        m_service = (NotificationProxyServiceManager) m_client
                .getServiceManagerContext();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        IwdsLog.i(TAG, "Notification - Disconnected unexpected:" + unexpected);
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
        IwdsLog.i(
                TAG,
                "Notification - Connect Failed reason:"
                        + reason.getReasonCode());
    }

    boolean enableVibrator = true;
    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            enableVibrator = true;
        };
    };

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (Intent.ACTION_DATE_CHANGED.equals(action)) {
                // 监听到系统日期改变，清除备份的消息
                if(mDbHelper != null){
                    int result = mDbHelper.notificationBackupDelete(null, null);
                    IwdsLog.d(TAG, "Backup notifications delete result " + result);
                }
            }
        }
    };
}
