/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.ingenic.iwds.utils.IwdsLog;

/**
 * 开机启动接收消息的服务
 * 
 * @author tZhang
 * 
 */
public class NotificationReceiver extends BroadcastReceiver {

    private Intent mNotifyIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        mNotifyIntent = new Intent(context, NotificationService.class);

        // 启动消息中心服务
        context.startService(mNotifyIntent);
        IwdsLog.i(this, "Receives the Notification service is started!");
    }

}
