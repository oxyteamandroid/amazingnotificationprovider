/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification.ancsclient;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;

import com.ingenic.ancsclient.AncsService;
import com.ingenic.iwds.app.Note;
import com.ingenic.iwds.app.NotificationProxyServiceManager;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.provider.notification.NotificationContrat;
import com.ingenic.provider.notification.NotificationDatabaseHelper;
import com.ingenic.provider.notification.utils.NotificationProviderUtils;

/**
 * Service for managing connection and data communication with a GATT server
 * hosted on a given Bluetooth LE device.
 */
public class BluetoothLeService extends AncsService implements
        ServiceClient.ConnectionCallbacks {
    final static String TAG = BluetoothLeService.class.getSimpleName();

    private static MessageSender sMessageSender = null;

    private ServiceClient m_client;
    private NotificationProxyServiceManager m_service;

    private Intent mStartIntent;
    private Intent mUpdateIntent;
    private NotificationDatabaseHelper mDbHelper;

    /** 清除通知栏IPONE消息的Note */
    private Note mNote = new Note("Clear Iphone Notifications", "iphone");

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (MessageSender.APPLE_CONNECT_STATE.equals(action)) {
                // 断开连接清除状态栏消息
                if ("disconnected".equals(intent.getStringExtra("connState"))) {
                    if (m_service != null) {
                        m_service.notify(-1, mNote);
                    }
                }
            } else if (MessageSender.ANCS_UPDATE.equals(action)) {
                NotificationInfo info = intent
                        .getParcelableExtra(MessageSender.EXTRA_ANCS_NOTIFICATION);
                if (null == info) {
                    IwdsLog.d(TAG, "Ancs receive notification is null!");
                    return;
                }

                long _id = -100;

                // 过滤重复数据
                if (mDbHelper != null && !mDbHelper.notificationHasData(info)) {

                    IwdsLog.i(TAG, "receive notification from iphone:" + info.toString());

                    // 保存数据到本地
                    _id = mDbHelper.notificationInsert(info);
                    info.id = _id;

                    // 更新本地页面数据
                    if (mUpdateIntent == null) {
                        mUpdateIntent = new Intent(
                                NotificationContrat.ACTION_NOTIFICATION_UPDATE);
                    mUpdateIntent.putExtra("notification", info);
                    sendBroadcast(mUpdateIntent);
                    }

                    // 不在消息中心界面时提示用户
                    if (!NotificationProviderUtils.isHome(context)) {
                        // 播放提示音
                        RingtoneManager
                                .getRingtone(
                                        context,
                                        RingtoneManager
                                                .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                .play();

                        // 震动
                        // Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                        // vibrator.vibrate(new long[] { 0, 500, 1000, }, -1);
                    }

                    if (null == mStartIntent) {
                        // 启动消息中心的Intent
                        mStartIntent = new Intent();
                        ComponentName componentName = new ComponentName(
                                "com.ingenic.notification",
                                "com.ingenic.notification.NotificationActivity");
                        mStartIntent.setComponent(componentName);
                    }

                    // 更新launcher
                    if (m_service != null) {
                        Note note = new Note(info.title, info.content,
                                PendingIntent.getActivity(
                                        context,
                                        Intent.FLAG_ACTIVITY_FORWARD_RESULT,
                                        mStartIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT));
                        m_service.notify((int) _id, note);
                    }
                }
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        sMessageSender = MessageSender.getInstance(this);
        setAncsSender(sMessageSender);

        mDbHelper = NotificationDatabaseHelper.getInstance(this);

        if (null != mReceiver) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(MessageSender.APPLE_CONNECT_STATE);
            filter.addAction(MessageSender.ANCS_UPDATE);
            registerReceiver(mReceiver, filter);
        }

        if (m_client == null) {
            m_client = new ServiceClient(this,
                    ServiceManagerContext.SERVICE_NOTIFICATION_PROXY, this);
            m_client.connect();
        }
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        m_service = (NotificationProxyServiceManager) m_client
                .getServiceManagerContext();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {

    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient,
            ConnectFailedReason reason) {
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != mReceiver) {
            unregisterReceiver(mReceiver);
        }
    }

}
