/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification.ancsclient;

import com.ingenic.iwds.utils.IwdsLog;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

public class BluetoothBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = BluetoothBroadcastReceiver.class.getSimpleName();
    private static final String ACTION_ANCSCLIENT_START_BLUETOOTH = "com.ingenic.ancsclient.action.START_BLUETOOTH";
    private static boolean sMsgRestartBluetooth;

    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                    BluetoothAdapter.ERROR);
            IwdsLog.v(TAG, "bluetooth state: " + state);
            if (BluetoothAdapter.STATE_ON == state) {
                // 启动ancs连接服务前，首先确保记录的ancs连接状态是断开的
                Settings.System.putInt(context.getContentResolver(), MessageSender.ANCS_CONNECT_STATE, 0);

                sMsgRestartBluetooth = false;
                context.startService(new Intent(context,
                        BluetoothLeService.class));
                IwdsLog.v(TAG, "ancs service is started!");
            } else if (BluetoothAdapter.STATE_OFF == state) {
                context.stopService(new Intent(context,
                        BluetoothLeService.class));
                IwdsLog.v(TAG, "ancs service is stoped!");
                if (sMsgRestartBluetooth) {
                    sMsgRestartBluetooth = false;
                    BluetoothManager bm = (BluetoothManager) context
                            .getSystemService(Context.BLUETOOTH_SERVICE);
                    if (bm == null)
                        return;
                    BluetoothAdapter ba = bm.getAdapter();
                    if (ba == null)
                        return;
                    ba.enable();
                }
            }
        } else if (ACTION_ANCSCLIENT_START_BLUETOOTH.equals(action)) {
            sMsgRestartBluetooth = true;
        }
    }
}
