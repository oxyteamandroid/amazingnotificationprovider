/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification.ancsclient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import com.ingenic.ancsclient.AncsSender;
import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.provider.notification.R;

public class MessageSender implements AncsSender {

    private static final String CALL_RING = "shenzhen.ingenic.ancs.comingcall.command";
    private static final String CALL_RING_STATE = "ringstate";
    private static final String CALL_NUMBER = "number";
    public static final String RECEIVER_ACTION_STATE_CHANGE = "receiver.action.STATE_CHANGE";
    public static final String EXTRA_ADDRESS = "extra_address";
    public static final String EXTRA_STATE = "extra_state";
    public static final String APPLE_CONNECT_STATE = "com.indroid.appleconnectstate";
    public static final String ANCS_CONNECT_STATE = "ANCS_CONNECT_STATE";
    public static final String ANCS_UPDATE = "con.ingenic.ANCS_UPDATE";
    public static final String EXTRA_ANCS_NOTIFICATION = "ancs_notification";
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    public static final String IPHONE = "iphone";
    private static String mAddr = null;
    private static String TAG = "MessageSender";

    private static final int CONN_STATE_ID = 0;
    private static final int ANCS_MESSAGE_UID_BASE = 100;

    public static final int STATE_DISCONNECTED = 1;
    public static final int STATE_CONNECTING = 2;
    public static final int STATE_CONNECTED = 3;

    private static final int CATEGORY_INCOMING_CALL = 1;

    private static final int EVENT_ADDED = 0;
    @SuppressWarnings("unused")
    private static final int EVENT_MODIFIED = 1;
    private static final int EVENT_REMOVED = 2;

    private boolean hasRing = false;

    private Context mContext;
    private static MessageSender sInstance;

    class category_equ_uid {
        public category_equ_uid() {
            category = -100;
            uid = -100;
        }

        public category_equ_uid(int cate, int id) {
            category = cate;
            uid = id;
        }

        @Override
        public boolean equals(Object obj) {
            category_equ_uid ceu = (category_equ_uid) obj;
            if ((this.category == (ceu.category)) && (this.uid == (ceu.uid)))
                return true;
            return false;
        }

        int category;
        int uid;
    };

    private ArrayList<category_equ_uid> mCategory_equ_uidlist = new ArrayList<category_equ_uid>();

    private MessageSender(Context context) {
        mContext = context;
    }

    public static MessageSender getInstance(Context context) {
        if (null == sInstance)
            sInstance = new MessageSender(context);
        return sInstance;
    }

    public void broadcastStaAddr(String addr) {
        IwdsLog.v(TAG, "addr: " + addr);
        if (addr.contains("00:00:00:00:00:00"))
            return;
        mAddr = addr;
    }

    public void broadcast(int eventUid, int eventFlags, int categoryId,
            int categoryCount, int notifUid) {
        IwdsLog.v(TAG, "sendNotif: " + eventUid);
        /*
        */
        if ((eventUid == EVENT_ADDED) && (categoryId == CATEGORY_INCOMING_CALL)) {
            IwdsLog.i("mk", "add a ringing msg....");
            mCategory_equ_uidlist.add(new category_equ_uid(
                    CATEGORY_INCOMING_CALL, notifUid));
            hasRing = true;
        }
        if ((eventUid == EVENT_REMOVED)
                && (categoryId == CATEGORY_INCOMING_CALL)) {
            Intent it = new Intent(CALL_RING);
            hasRing = false;
            it.putExtra(CALL_RING_STATE, "ended");
            it.putExtra(CALL_NUMBER, "");
            mContext.sendBroadcast(it);
        }

        NotificationManager nm = ((NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE));
        if (eventUid == 2) {
            nm.cancel(ANCS_MESSAGE_UID_BASE + notifUid);
        } else {
            /*
             * String message = "you have " + mCategoryCount + " " +
             * EventFlagArray[mEventFlags] + " " + category;
             * Notification.Builder build = new Notification.Builder(c)
             * .setSmallIcon(R.drawable.ic_launcher)
             * .setContentTitle(mNotificationUID + " ")
             * .setContentText(message);
             * nm.notify(MessageSender.ANCS_MESSAGE_UID_BASE + mNotificationUID,
             * build.build());
             */
        }

    }

//    @Override
    public void broadcast(int uid/*, String appName*/, String title, String message, String date,
            String subtitle, String messageSize, boolean isEmail) {
        if (hasRing == true) {
            IwdsLog.i("mk", "send a ringing msg ....");
            if ((mCategory_equ_uidlist.contains(new category_equ_uid(
                    CATEGORY_INCOMING_CALL, uid)))) {
                mCategory_equ_uidlist.remove(new category_equ_uid(
                        CATEGORY_INCOMING_CALL, uid));
                if (mCategory_equ_uidlist.isEmpty()) {
                    IwdsLog.i("mk", "no more ringing msg waiting for send.");
                    hasRing = false;
                }
                Intent mIntent = new Intent(CALL_RING);
                mIntent.putExtra(CALL_RING_STATE, "ringing");
                mIntent.putExtra(CALL_NUMBER, title);
                mContext.sendBroadcast(mIntent);
            }
        } else {
            Notification.Builder build = new Notification.Builder(mContext)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle(title).setContentText(message);
            ((NotificationManager) mContext
                    .getSystemService(Context.NOTIFICATION_SERVICE)).notify(uid
                    + ANCS_MESSAGE_UID_BASE, build.build());

            long time = System.currentTimeMillis();
            IwdsLog.d(TAG, /*"appName: " + appName + */", title: " + title + ", message: " + message + ", date: " + date + ", time: " + time);

            NotificationInfo info = new NotificationInfo();
            info.packageName = IPHONE;
            info.appName = title;//appName;
            info.title = title;
            info.content = message;
            info.read = 1;
            String[] ds = date.split("T");
            date = ds[0] + ds[1];
            try {
                info.updateTime = mDateFormat.parse(date).getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Intent mIntent = new Intent();
            mIntent.setAction(ANCS_UPDATE);
            mIntent.putExtra(EXTRA_ANCS_NOTIFICATION, info);
            mContext.sendBroadcast(mIntent);
        }
    }

    public void broadcast(String devName, String addr, int connState) {
        IwdsLog.v(TAG, "broadcast addr: " + addr + " connstate: " + connState);
        NotificationManager mgr = (NotificationManager) mContext
                .getSystemService(Context.NOTIFICATION_SERVICE);
        mgr.cancel(CONN_STATE_ID);
        String title = "";
        int icon = -1;
        String message = "";
        if (connState == STATE_CONNECTING) {
            mCategory_equ_uidlist.clear();
            title = "Connecting...";
            icon = R.drawable.connecting_state;
            message = addr + " Connecting...";
        } else if (connState == STATE_CONNECTED) {
            title = "Connected.";
            icon = R.drawable.connected_state;
            message = addr + " Connected.";

            Intent it = new Intent(APPLE_CONNECT_STATE);
            it.putExtra("devName", devName);
            it.putExtra("addr", mAddr);
            it.putExtra("connState", "connected");
            Settings.System.putInt(mContext.getContentResolver(),
                    ANCS_CONNECT_STATE, 1);
            mContext.sendBroadcast(it);
            IwdsLog.v(TAG, "connected device addr: " + mAddr);
            Intent intent = new Intent(RECEIVER_ACTION_STATE_CHANGE);
            intent.putExtra(EXTRA_STATE, 12);
            intent.putExtra(EXTRA_ADDRESS, mAddr);
            mContext.sendBroadcast(intent);
        } else if (connState == STATE_DISCONNECTED) {
            Intent it = new Intent(APPLE_CONNECT_STATE);
            it.putExtra("devName", devName);
            it.putExtra("addr", mAddr);
            it.putExtra("connState", "disconnected");
            Settings.System.putInt(mContext.getContentResolver(),
                    ANCS_CONNECT_STATE, 0);
            mContext.sendBroadcast(it);
        }

        if (!title.equals("")) {
            Notification.Builder build = new Notification.Builder(mContext);
            build.setSmallIcon(icon).setContentTitle(title);
            build.setContentText(message);
            build.setOngoing(true);
            mgr.notify(CONN_STATE_ID, build.build());
        }
    }

}
