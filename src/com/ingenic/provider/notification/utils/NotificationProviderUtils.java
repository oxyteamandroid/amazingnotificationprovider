/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider/ Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification.utils;

import android.app.ActivityManager;
import android.content.Context;

/**
 * @author tZhang
 */

public class NotificationProviderUtils {

    private static final String HOME_NOTIFICATION = "com.ingenic.notification";

    /**
     * 判断当前界面是否是消息中心
     */
    @SuppressWarnings("deprecation")
    public static boolean isHome(Context context) {
        ActivityManager mActivityManager = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);

        try {
            String packageName = mActivityManager.getRunningTasks(1).get(0).topActivity
                    .getPackageName();
            return HOME_NOTIFICATION.equals(packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
