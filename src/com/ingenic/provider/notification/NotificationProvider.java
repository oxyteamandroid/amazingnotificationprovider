/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification;

import java.util.Arrays;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateUtils;
import android.util.Log;

public class NotificationProvider extends SQLiteContentProvider {

    private static final String TAG = "NotificationProvider";
    private static final int NOTIFICATIONS = 1;
    private static final int NOTIFICATIONS_ID = 2;
    private static final int CALSSIFICATIONS = 3;
    private static final int UPDATE_BROADCAST_MSG = 1;
    private static final long UPDATE_BROADCAST_TIMEOUT_MILLIS = DateUtils.SECOND_IN_MILLIS;
    private static final long SYNC_UPDATE_BROADCAST_TIMEOUT_MILLIS = 30 * DateUtils.SECOND_IN_MILLIS;
    public static final String QUERY_TABLE_NOTIFICATION = "(SELECT * FROM NOTIFICATION ORDER BY "
            + NotificationContrat.Notifications._ID + " DESC)";

    public static final String QUERY_TABLE_CALSSIFICATION = "(SELECT TITLE, COUNT(*) AS NUMBER FROM NOTIFICATION WHERE 0=0 GROUP BY TITLE) AS A"
            + " LEFT JOIN (SELECT TITLE, COUNT(*) AS UNREAD_NUM FROM NOTIFICATION WHERE READED=1 GROUP BY TITLE) AS B ON A.TITLE = B.TITLE";

    private static final UriMatcher sUriMatcher = new UriMatcher(
            UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(NotificationContrat.AUTHORITY, "notifications",
                NOTIFICATIONS);
        sUriMatcher.addURI(NotificationContrat.AUTHORITY, "notifications/#",
                NOTIFICATIONS_ID);
        sUriMatcher.addURI(NotificationContrat.AUTHORITY, "calssifications",
                CALSSIFICATIONS);
    }

    private Context mContext;

    private final Handler mBroadcastHandler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            if (msg.what == UPDATE_BROADCAST_MSG) {
                doSendUpdateNotification();

                // mContext.stopService(new Intent(mContext,
                // EmptyService.class));
            }
        };
    };

    private NotificationDatabaseHelper mDBHelper;

    protected static final String SQL_WHERE_ID = NotificationContrat.Notifications._ID
            + "=?";

    @Override
    public boolean onCreate() {
        super.onCreate();
        return initialize();
    }

    private boolean initialize() {
        mContext = getContext();
        mDBHelper = (NotificationDatabaseHelper) getDatabaseHelper();
        return true;
    }

    @Override
    protected SQLiteOpenHelper getDatabaseHelper(Context context) {
        return NotificationDatabaseHelper.getInstance(context);
    }

    @Override
    protected Uri insertInTransaction(Uri uri, ContentValues values,
            boolean callerIsSyncAdapter) {
        final int match = sUriMatcher.match(uri);
        long id = 0;
        switch (match) {
        case NOTIFICATIONS:
            id = mDBHelper.notificationInsert(values);

            if (id != -1) {
                sendUpdateNotification(id, callerIsSyncAdapter);
            }
            break;
        case NOTIFICATIONS_ID:
            throw new UnsupportedOperationException(
                    "Cannot insert into that URL: " + uri);
        }
        if (id < 0) {
            return null;
        }
        return ContentUris.withAppendedId(uri, id);
    }

    private void sendUpdateNotification(long notificationsId,
            boolean callerIsSyncAdapter) {
        // Are there any pending broadcast requests?
        if (mBroadcastHandler.hasMessages(UPDATE_BROADCAST_MSG)) {
            // Delete any pending requests, before requeuing a fresh one
            mBroadcastHandler.removeMessages(UPDATE_BROADCAST_MSG);
        } else {
            // Because the handler does not guarantee message delivery in
            // the case that the provider is killed, we need to make sure
            // that the provider stays alive long enough to deliver the
            // notification. This empty service is sufficient to "wedge" the
            // process until we stop it here.
            // mContext.startService(new Intent(mContext, EmptyService.class));
        }
        // We use a much longer delay for sync-related updates, to prevent any
        // receivers from slowing down the sync
        long delay = callerIsSyncAdapter ? SYNC_UPDATE_BROADCAST_TIMEOUT_MILLIS
                : UPDATE_BROADCAST_TIMEOUT_MILLIS;
        // Despite the fact that we actually only ever use one message at a time
        // for now, it is really important to call obtainMessage() to get a
        // clean instance. This avoids potentially infinite loops resulting
        // adding the same instance to the message queue twice, since the
        // message queue implements its linked list using a field from Message.
        Message msg = mBroadcastHandler.obtainMessage(UPDATE_BROADCAST_MSG);
        mBroadcastHandler.sendMessageDelayed(msg, delay);
    }

    private void doSendUpdateNotification() {
        Intent it = new Intent(Intent.ACTION_PROVIDER_CHANGED,
                NotificationContrat.CONTENT_URI);
        mContext.sendBroadcast(it, null);
    }

    @Override
    protected int updateInTransaction(Uri uri, ContentValues values,
            String selection, String[] selectionArgs,
            boolean callerIsSyncAdapter) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
        case NOTIFICATIONS:
        case NOTIFICATIONS_ID:
            Cursor notifications = null;

            try {
                if (match == NOTIFICATIONS_ID) {
                    long id = ContentUris.parseId(uri);
                    notifications = mDb.query(
                            NotificationDatabaseHelper.TABLE_NOTIFICATION,
                            null, SQL_WHERE_ID,
                            new String[] { String.valueOf(id) }, null, null,
                            null);
                } else {
                    notifications = mDb.query(
                            NotificationDatabaseHelper.TABLE_NOTIFICATION,
                            null, selection, selectionArgs, null, null, null);
                }

                if (notifications.getCount() == 0) {
                    Log.i(TAG, "No events to update: uri=" + uri
                            + " selection=" + selection + " selectionArgs="
                            + Arrays.toString(selectionArgs));
                    return 0;
                }

                return handleUpdateEvents(notifications, values, selection,
                        selectionArgs, callerIsSyncAdapter);
            } finally {
                if (notifications != null) {
                    notifications.close();
                }
            }
        default:
            throw new IllegalArgumentException("Unknown Uri：" + uri);
        }
    }

    private int handleUpdateEvents(Cursor cursor, ContentValues updateValues,
            String selection, String[] selectionArgs,
            boolean callerIsSyncAdapter) {
        if (cursor.getCount() > 1) {
            Log.d(TAG, "Performing update on " + cursor.getCount()
                    + " notifications");
        }

        // 更新操作的代码块
        mDBHelper.notificationUpdate(updateValues, selection, selectionArgs);

        return 0;
    }

    @Override
    protected int deleteInTransaction(Uri uri, String selection,
            String[] selectionArgs, boolean callerIsSyncAdapter) {
        // 删除操作的代码块
        int result = mDBHelper.notificationDelete(selection, selectionArgs);

        return result;
    }

    @Override
    protected void notifyChange(boolean syncToNetwork) {

    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
            String[] selectionArgs, String sortOrder) {

        Cursor cursor;
        mDb = mDBHelper.getReadableDatabase();
        switch (sUriMatcher.match(uri)) {
        case NOTIFICATIONS:
            cursor = mDb.query(QUERY_TABLE_NOTIFICATION, projection, selection,
                    selectionArgs, null, null, sortOrder);
            break;
        case NOTIFICATIONS_ID:
            long id = ContentUris.parseId(uri);
            cursor = mDb.query(QUERY_TABLE_NOTIFICATION, projection,
                    SQL_WHERE_ID, new String[] { String.valueOf(id) }, null,
                    null, sortOrder);
            break;
        case CALSSIFICATIONS:
            cursor = mDb.query(QUERY_TABLE_CALSSIFICATION, projection,
                    selection, selectionArgs, null, null, sortOrder);
            break;
        default:
            throw new IllegalArgumentException("Unknown Uri：" + uri);
        }
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }
}