/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/AmazingNotificationProvider Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.provider.notification;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ingenic.iwds.datatransactor.elf.NotificationInfo;
import com.ingenic.provider.notification.ancsclient.MessageSender;

/**
 * 消息通知数据库助手
 * 
 * @author tZhang
 */
public class NotificationDatabaseHelper extends SQLiteOpenHelper {
    public static final String TABLE_NOTIFICATION = "notification";
    public static final String TABLE_NOTIFICATION_BACKUP = "notification_backup";
    private static final String DATABASE_NAME = "notification.db";
    private static final int DATABASE_VERSION = 1;
    private static final String SQL_CREATE = "CREATE TABLE IF NOT EXISTS ";
    private static final String SQL_DROP = "DROP TABLE IF EXISTS ";
    private static final String SQL_WHERE_UPDATE_TIME = NotificationContrat.Notifications.UPDATE_TIME
            + " = ? ";
    private static NotificationDatabaseHelper sIntance;
    private SQLiteDatabase mDb;
    private ContentValues mValues = new ContentValues();

    public static synchronized NotificationDatabaseHelper getInstance(
            Context context) {
        if (sIntance == null) {
            sIntance = new NotificationDatabaseHelper(context);
        }
        return sIntance;
    }

    private NotificationDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE + TABLE_NOTIFICATION + " ("
                + NotificationContrat.Notifications._ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NotificationContrat.Notifications.APPNAME + " TEXT, "
                + NotificationContrat.Notifications.PACKAGENAME + " TEXT, "
                + NotificationContrat.Notifications.TITLE + " TEXT, "
                + NotificationContrat.Notifications.MESSAGE + " TEXT, "
                + NotificationContrat.Notifications.UPDATE_TIME + " INTEGER, "
                + NotificationContrat.Notifications.READED + " INTEGER);");

        // COPY TABLE_NOTIFICATION --> BACKUP
        db.execSQL(SQL_CREATE + TABLE_NOTIFICATION_BACKUP + " AS SELECT * FROM "
                + TABLE_NOTIFICATION + " WHERE 1=0");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // 更新数据库版本
        db.execSQL(SQL_DROP + TABLE_NOTIFICATION);
        db.execSQL(SQL_DROP + TABLE_NOTIFICATION_BACKUP);

        onCreate(db);
    }

    /**
     * Insert Notification(插入消息通知)
     * 
     * @param values
     *            this map contains the initial column values for the row. The
     *            keys should be the column names and the values the column
     *            values(一个ContentValues对象，类似一个map.通过键值对的形式存储值。)
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long notificationInsert(ContentValues values) {
        mDb = getWritableDatabase();
        return mDb.insert(TABLE_NOTIFICATION, null, values);
    }

    /**
     * Insert Notification(备份消息通知)
     *
     * @param values
     *            this map contains the initial column values for the row. The
     *            keys should be the column names and the values the column
     *            values(一个ContentValues对象，类似一个map.通过键值对的形式存储值。)
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long notificationBackupInsert(ContentValues values) {
        mDb = getWritableDatabase();
        return mDb.insert(TABLE_NOTIFICATION_BACKUP, null, values);
    }

    /**
     * Delete Notification (删除消息通知)
     * 
     * @param whereClause
     *            the optional WHERE clause to apply when deleting. Passing null
     *            will delete all rows.(可选的where语句)
     * @param whereArgs
     *            (whereClause语句中表达式的？占位参数列表)
     * @return the number of rows affected if a whereClause is passed in, 0
     *         otherwise. To remove all rows and get a count pass "1" as the
     *         whereClause.(删除操作执行后受影响的行数)
     */
    public int notificationDelete(String whereClause, String[] whereArgs) {
        mDb = getWritableDatabase();
        return mDb.delete(TABLE_NOTIFICATION, whereClause, whereArgs);
    }

    /**
     * Delete Notification (删除备份的消息通知)
     *
     * @param whereClause
     *            the optional WHERE clause to apply when deleting. Passing null
     *            will delete all rows.(可选的where语句)
     * @param whereArgs
     *            (whereClause语句中表达式的？占位参数列表)
     * @return the number of rows affected if a whereClause is passed in, 0
     *         otherwise. To remove all rows and get a count pass "1" as the
     *         whereClause.(删除操作执行后受影响的行数)
     */
    public int notificationBackupDelete(String whereClause, String[] whereArgs) {
        mDb = getWritableDatabase();
        return mDb.delete(TABLE_NOTIFICATION_BACKUP, whereClause, whereArgs);
    }

    /**
     * Update Notification (更新消息通知)
     * 
     * @param values
     *            a map from column names to new column values. null is a valid
     *            value that will be translated to
     *            NULL.(一个ContentValues对象，类似一个map.通过键值对的形式存储值。)
     * @param whereClause
     *            the optional WHERE clause to apply when updating. Passing null
     *            will update all rows.(可选的where语句)
     * @param whereArgs
     *            the group of args to deal with(whereClause语句中表达式的？占位参数列表)
     * @return the number of rows affected.(更新操作后受影响的行数)
     */
    public int notificationUpdate(ContentValues values, String whereClause,
            String[] whereArgs) {
        mDb = getWritableDatabase();
        return mDb.update(TABLE_NOTIFICATION, values, whereClause, whereArgs);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        mDb = db;
    }

    /**
     * 插入一条通知到数据库
     *
     * @param info
     *            通知内容
     * @return 插入记录的ID
     */
    public long notificationInsert(NotificationInfo info) {
        if (info != null) {
            mValues.clear();
            mValues.put(NotificationContrat.Notifications.APPNAME, info.appName);
            mValues.put(NotificationContrat.Notifications.PACKAGENAME,
                    info.packageName);
            mValues.put(NotificationContrat.Notifications.TITLE, info.title);
            mValues.put(NotificationContrat.Notifications.MESSAGE, info.content);
            mValues.put(NotificationContrat.Notifications.UPDATE_TIME,
                    info.updateTime);
            mValues.put(NotificationContrat.Notifications.READED, 1); // 默认新收到的消息都是未读消息

            if(MessageSender.IPHONE.equals(info.packageName)){
                // 备份来自Iphone的消息，防止重复接收Iphone的消息
                notificationBackupInsert(mValues);
            }

            return notificationInsert(mValues);
        }

        return -1;
    }

    /**
     * 判断是否为重复数据
     * @param info
     * @return
     */
    public synchronized boolean notificationHasData(NotificationInfo info) {
        mDb = getReadableDatabase();
        Cursor cursor = mDb.query(TABLE_NOTIFICATION_BACKUP, null,
                SQL_WHERE_UPDATE_TIME, new String[] { info.updateTime + "" },
                null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.close();
            return true;
        }

        if(cursor!= null && cursor.getCount() == 0){
            cursor.close();
        }

        return false;
    }
}
